# On importe les librairies dont on aura besoin pour ce tp
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
feature_cols = [
    'turn',
    'game',
    'pacman_x',
    'pacman_y',
    'Blinky_x',
    'Blinky_y',
    'Blinky_scared_timer',
    'Pinky_x',
    'Pinky_y',
    'Pinky_scared_timer',
    'food_number',
    'nearest_food_x',
    'nearest_food_y',
    'nearest_capsule_x',
    'nearest_capsule_y',
    'north',
    'south',
    'east',
    'west',
    'score',
    'game_won'
]

# On charge le dataset

"""
dataset.plot(x='', y='', style='o')
plt.xlabel('')
plt.ylabel('')
plt.show()
"""