# multiAgents.py
# --------------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html
from sklearn import naive_bayes
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score

from util import manhattanDistance
from game import Directions
import random
from pandas import DataFrame, read_csv
import random, util
import time
from game import Agent
from sklearn.linear_model import LogisticRegression
from util import manhattanDistance

GHOST_IMPORTANCE_RATIO = 0.4
FOOD_IMPORTANCE_RATIO = 1.2
CAPSULE_IMPORTANCE_RATIO = 1.0
SCORE_IMPORTANCE_RATIO = 10.0
REMAINING_FOODS_IMPORTANCE_RATIO = 7.0


class ReflexAgent(Agent):
    """
      A reflex agent chooses an action at each choice point by examining
      its alternatives via a state evaluation function.

      The code below is provided as a guide.  You are welcome to change
      it in any way you see fit, so long as you don't touch our method
      headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {North, South, West, East, Stop}
        """
        # Collect legal moves and successor states

        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"
        return legalMoves[chosenIndex]

    def evaluationFunction(self, currGameState, pacManAction):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (oldFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)

        successorGameState = currGameState.generatePacmanSuccessor(pacManAction)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newFoodList = newFood.asList()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

        distanceToFood = [1.0 / manhattanDistance(x, newPos) for x in newFoodList]
        scoreBasedOnFood = max(distanceToFood + [0])

        distanceToGhost = [pow(max(8 - manhattanDistance(x.getPosition(), newPos), 0), 2) for x in newGhostStates]
        ghostCoefficients = [-1 if x.scaredTimer <= 0 else 1 for x in newGhostStates]
        scoreBasedOnGhosts = sum(map(lambda x,y: x * y, distanceToGhost, ghostCoefficients))



        return scoreBasedOnFood + scoreBasedOnGhosts + successorGameState.getScore()


def scoreEvaluationFunction(currentGameState):
    """
      This default evaluation function just returns the score of the state.
      The score is the same one displayed in the Pacman GUI.

      This evaluation function is meant for use with adversarial search agents
      (not reflex agents).
    """
    GhostStates = currentGameState.getGhostStates()  # all the ghost states
    Pacman_Pos = currentGameState.getPacmanPosition()
    food_list = (currentGameState.getFood()).asList()  # get all the food as list.
    capsule_list = currentGameState.getCapsules()  # get all the capsules.
    no_food = len(food_list)
    no_capsule = len(capsule_list)

    score = 0
    if currentGameState.getNumAgents() > 1:
        ghost_dis = min([manhattanDistance(Pacman_Pos, ghost.getPosition()) for ghost in GhostStates])
        if (ghost_dis <= 1):
            return -10000
        score -= GHOST_IMPORTANCE_RATIO / ghost_dis

    current_food = Pacman_Pos
    for food in food_list:
        closestFood = min(food_list, key=lambda x: manhattanDistance(x, current_food))
        score += FOOD_IMPORTANCE_RATIO / (manhattanDistance(current_food, closestFood))
        current_food = closestFood
        food_list.remove(closestFood)

    current_capsule = Pacman_Pos
    for capsule in capsule_list:
        closest_capsule = min(capsule_list, key=lambda x: manhattanDistance(x, current_capsule))
        score += CAPSULE_IMPORTANCE_RATIO / (manhattanDistance(current_capsule, closest_capsule))
        current_capsule = closest_capsule
        capsule_list.remove(closest_capsule)

    score += SCORE_IMPORTANCE_RATIO * (currentGameState.getScore())

    score -= REMAINING_FOODS_IMPORTANCE_RATIO * (no_food + no_capsule)
    end = time.time()

    return score




class MultiAgentSearchAgent(Agent):
    """
      This class provides some common elements to all of your
      multi-agent searchers.  Any methods defined here will be available
      to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

      You *do not* need to make any changes here, but you can if you want to
      add functionality to all your adversarial search agents.  Please do not
      remove anything, however.

      Note: this is an abstract class: one that should not be instantiated.  It's
      only partially specified, and designed to be extended.  Agent (game.py)
      is another abstract class.
    """

    def __init__(self, evalFn='scoreEvaluationFunction', depth='2'):
        Agent.__init__(self)
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)


class MinimaxAgent(MultiAgentSearchAgent):

    def getAction(self, gameState):
        """
          Returns the expectimax action using self.depth and self.evaluationFunction
          All ghosts should be modeled as choosing uniformly at random from their
          legal moves.
        """
        pacman_legal_actions = gameState.getLegalActions(0)
        max_value = float('-inf')
        max_action = None

        for action in pacman_legal_actions:
            action_value = self.Pmin(gameState.generateSuccessor(0, action), 1, 0)
            if action_value > max_value:
                max_value = action_value
                max_action = action

        return max_action


    def Pmax(self, gameState, depth):
        if depth == self.depth or len(gameState.getLegalActions(0)) == 0:
            return self.evaluationFunction(gameState)

        return max(
            [
                self.Pmin(gameState.generateSuccessor(0, action), 1, depth) for action in gameState.getLegalActions(0)
            ]
        )


    def Pmin(self, gameState, agentIndex, depth):
        """ For the MIN Players or Agents  """

        num_actions = len(gameState.getLegalActions(agentIndex))

        if num_actions == 0:
            return self.evaluationFunction(gameState)

        if agentIndex < gameState.getNumAgents() - 1:
            return sum(
                [
                    self.Pmin(gameState.generateSuccessor(agentIndex, action), agentIndex + 1, depth) for action in
                    gameState.getLegalActions(agentIndex)
                ]
            ) / float(num_actions)

        else:
            return sum(
                [
                    self.Pmax(gameState.generateSuccessor(agentIndex, action), depth + 1) for action in
                    gameState.getLegalActions(agentIndex)
                ]
            ) / float(num_actions)


class AlphaBetaAgent(MultiAgentSearchAgent):
    """
      Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action using self.depth and self.evaluationFunction
        """
        depth = 0
        alpha = float('-inf')
        beta = float('inf')
        return self.getMaxValue(gameState, alpha, beta, depth)[1]

    def getMaxValue(self, gameState, alpha, beta, depth, agent=0):
        actions = gameState.getLegalActions(agent)

        if not actions or gameState.isWin() or depth >= self.depth:
            return self.evaluationFunction(gameState), Directions.STOP

        successorCost = float('-inf')
        successorAction = Directions.STOP

        for action in actions:
            successor = gameState.generateSuccessor(agent, action)

            cost = self.getMinValue(successor, alpha, beta, depth, agent + 1)[0]

            if cost > successorCost:
                successorCost = cost
                successorAction = action

            if successorCost > beta:
                return successorCost, successorAction

            alpha = max(alpha, successorCost)

        return successorCost, successorAction

    def getMinValue(self, gameState, alpha, beta, depth, agent):
        actions = gameState.getLegalActions(agent)

        if not actions or gameState.isLose() or depth >= self.depth:
            return self.evaluationFunction(gameState), Directions.STOP

        successorCost = float('inf')
        successorAction = Directions.STOP

        for action in actions:
            successor = gameState.generateSuccessor(agent, action)

            cost = 0

            if agent == gameState.getNumAgents() - 1:
                cost = self.getMaxValue(successor, alpha, beta, depth + 1)[0]
            else:
                cost = self.getMinValue(successor, alpha, beta, depth, agent + 1)[0]

            if cost < successorCost:
                successorCost = cost
                successorAction = action

            if successorCost < alpha:
                return successorCost, successorAction

            beta = min(beta, successorCost)

        return successorCost, successorAction


class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState):
        """
          Returns the expectimax action using self.depth and self.evaluationFunction
          All ghosts should be modeled as choosing uniformly at random from their
          legal moves.
        """

        depth = 0
        return self.getMaxValue(gameState, depth)[1]

    def getMaxValue(self, gameState, depth, agent=0):
        actions = gameState.getLegalActions(agent)
        if Directions.STOP in actions:
            actions.remove(Directions.STOP)

        if not actions or gameState.isWin() or depth >= self.depth:
            return self.evaluationFunction(gameState), actions

        successorCost = float('-inf')
        successorAction = actions

        for action in actions:
            successor = gameState.generateSuccessor(agent, action)

            cost = self.getMinValue(successor, depth, agent + 1)[0]

            if cost > successorCost:
                successorCost = cost
                successorAction = action

        return successorCost, successorAction

    def getMinValue(self, gameState, depth, agent):

        actions = gameState.getLegalActions(agent)
        if Directions.STOP in actions:
            actions.remove(Directions.STOP)

        if not actions or gameState.isLose() or depth >= self.depth:
            return self.evaluationFunction(gameState), None

        successorCosts = []

        for action in actions:

            successor = gameState.generateSuccessor(agent, action)

            cost = 0

            if agent == gameState.getNumAgents() - 1:
                cost = self.getMaxValue(successor, depth + 1)[0]
            else:
                cost = self.getMinValue(successor, depth, agent + 1)[0]

            successorCosts.append(cost)

        return sum(successorCosts) / float(len(successorCosts)), None


class TrainingAgent(ExpectimaxAgent):
    depth = 3
    game = 1
    round = 1
    victory = False
    df = None

    def __init__(self, index=0):
        ExpectimaxAgent.__init__(self)
        self.count = 0
        self.cols = [
            'decision',
            'turn',
            'game',
            'Blinky_dist',
            'Blinky_scared_timer',
            'Pinky_dist',
            'Pinky_scared_timer',
            'food_number',
            'nearest_food_dist',
            'nearest_capsule_dist',
            'north',
            'south',
            'east',
            'west',
            'score',
            'game_won'
        ]
        self.df = DataFrame(columns=self.cols)



    def getAction(self, GameState):
        pos = GameState.getPacmanPosition()
        blinky = GameState.getGhostStates()[0]
        pinky = GameState.getGhostStates()[1]

        food_list = GameState.getFood().asList()
        current_food = pos

        closestFood = (0,0)
        for food in food_list:
            try:
                closestFood = min(food_list, key=lambda x: manhattanDistance(x, current_food))
            except ValueError:
                break
            current_food = closestFood
            food_list.remove(closestFood)


        current_food = pos
        capsule_list = GameState.getCapsules()
        closestCaps = (0,0)
        for food in capsule_list:
            try:
                closestCaps = min(food_list, key=lambda x: manhattanDistance(x, current_food))
            except ValueError:
                break
            current_food = closestCaps
            food_list.remove(closestCaps)

        legal_moves = GameState.getLegalActions()

        action = ExpectimaxAgent.getAction(self, GameState)
        Decision= {
            "North": 1,
            "South": 2,
            "East": 3,
            "West": 4,
            "Stop": 5
        }

        Revert = [
            "",
            "North",
            "South",
            "East",
            "West",
            "Stop"
        ]

        current_line = [
            Decision[action],
            self.round,
            self.game,
            manhattanDistance(pos, blinky.getPosition()),
            blinky.scaredTimer,
            manhattanDistance(pos, pinky.getPosition()),
            pinky.scaredTimer,
            len(food_list),
            manhattanDistance(pos, closestFood),
            manhattanDistance(pos, closestCaps),
            1 if "North" in legal_moves else 0,
            1 if "South" in legal_moves else 0,
            1 if "East" in legal_moves else 0,
            1 if "West" in legal_moves else 0,
            GameState.getScore(),
            0
        ]
        self.df.loc[self.count, :] = current_line
        self.count += 1

        win = GameState.generatePacmanSuccessor(action).isWin()
        lose = GameState.generatePacmanSuccessor(action).isLose()
        if win or lose:
            self.victory = True
            if win:
                for i, row in self.df.iterrows():
                    self.df.loc[i, "game_won"] = 1
            else:
                for i, row in self.df.iterrows():
                    self.df.loc[i, "game_won"] = 0
            self.df.to_csv('dataframe.csv', mode='a', header=False)
            self.df = DataFrame(columns=self.cols)

        return action


class MachineLearningAgentSVC(ExpectimaxAgent):
    game = 1
    victory = False
    round = 1
    Decision = {
        "North": 1,
        "South": 2,
        "East": 3,
        "West": 4,
        "Stop": 5
    }

    Revert = [
        "",
        "North",
        "South",
        "East",
        "West",
        "Stop"
    ]

    def __init__(self):
        ExpectimaxAgent.__init__(self)
        self.dataset = read_csv('dataframe.csv')
        self.game = self.dataset['game'].max()
        X = self.dataset.loc[:, [
            'turn',
            'game',
            'Blinky_dist',
            'Blinky_scared_timer',
            'Pinky_dist',
            'Pinky_scared_timer',
            'food_number',
            'nearest_food_dist',
            'nearest_capsule_dist',
            'north',
            'south',
            'east',
            'west',
            'score',
            'game_won'
        ]]
        y = self.dataset.decision

        # 2. instantiate model
        self.logreg = SVC()
        #print(X)
        print('-------------------')
        #print(y)

        # 3. Split the dataset in two parts
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=0)
        feature_scaler = StandardScaler()
        X_train = feature_scaler.fit_transform(X_train)
        X_test = feature_scaler.transform(X_test)

        classifier = RandomForestClassifier(n_estimators=100, random_state=0)
        all_accuracies = cross_val_score(estimator=classifier, X=X_train, y=y_train, cv=5)
        print("precision des 5 crossvalidations:")
        print(all_accuracies)
        print("Moyenne des crossvalidation")
        print(all_accuracies.mean())
        print("écrart:")
        print(all_accuracies.std())

        grid_param = {
            'n_estimators': [100, 300, 500, 800, 1000],
            'criterion': ['gini', 'entropy'],
            'bootstrap': [True, False]
        }

        gd_sr = GridSearchCV(estimator=classifier,
                             param_grid=grid_param,
                             scoring='accuracy',
                             cv=3,
                             n_jobs=-1)
        gd_sr.fit(X_train, y_train)

        best_parameters = gd_sr.best_params_
        print("Best parameters :")
        print(best_parameters)

        best_result = gd_sr.best_score_
        print("Best result :")
        print(best_result)
        # 3. fit
        self.logreg.fit(X, y)

    def getCurrentLine(self, GameState):
        pos = GameState.getPacmanPosition()
        blinky = GameState.getGhostPosition(1)
        pinky = GameState.getGhostPosition(2)
        food_list = GameState.getFood().asList()
        closestFood = (0, 0)
        closestFood = min(food_list, key=lambda x: manhattanDistance(x, pos))
        closestCaps = (0, 0)
        closestCaps = min(food_list, key=lambda x: manhattanDistance(x, pos))
        legal_moves = GameState.getLegalActions()

        current_line = [
            self.round,
            self.game,
            manhattanDistance(pos, blinky),
            GameState.getGhostState(1).scaredTimer,
            manhattanDistance(pos, pinky),
            GameState.getGhostState(2).scaredTimer,
            len(food_list),
            manhattanDistance(pos, closestFood),
            manhattanDistance(pos, closestCaps),
            "North" in legal_moves,
            "South" in legal_moves,
            "East" in legal_moves,
            "West" in legal_moves,
            GameState.getScore(),
            False
        ]

        return current_line

    def getAction(self, GameState):
        self.round += 1
        current_line = self.getCurrentLine(GameState)

        index = self.mlcheck(current_line)
        action = self.Revert[int(index[0])]
        if not action in GameState.getLegalActions():
            action = ExpectimaxAgent.getAction(self, GameState)

        win = GameState.generatePacmanSuccessor(action).isWin()
        if win :
            self.victory = True
        return action

    def mlcheck(self, line):
        new = DataFrame(columns=[
            'turn',
            'game',
            'Blinky_dist',
            'Blinky_scared_timer',
            'Pinky_dist',
            'Pinky_scared_timer',
            'food_number',
            'nearest_food_dist',
            'nearest_capsule_dist',
            'north',
            'south',
            'east',
            'west',
            'score',
            'game_won'
        ])
        new.loc[0, :] = line

        new.head()
        return self.logreg.predict(new)


class MachineLearningAgentKNN(ExpectimaxAgent):
    game = 1
    victory = False
    round = 1
    Decision = {
        "North": 1,
        "South": 2,
        "East": 3,
        "West": 4,
        "Stop": 5
    }

    Revert = [
        "",
        "North",
        "South",
        "East",
        "West",
        "Stop"
    ]

    def __init__(self):
        ExpectimaxAgent.__init__(self)
        self.dataset = read_csv('dataframe.csv')
        self.game = self.dataset['game'].max()
        X = self.dataset.loc[:, [
            'turn',
            'game',
            'Blinky_dist',
            'Blinky_scared_timer',
            'Pinky_dist',
            'Pinky_scared_timer',
            'food_number',
            'nearest_food_dist',
            'nearest_capsule_dist',
            'north',
            'south',
            'east',
            'west',
            'score',
            'game_won'
        ]]
        y = self.dataset.decision

        # 2. instantiate model
        self.logreg = KNeighborsClassifier()
        #print(X)
        print('-------------------')
        #print(y)

        ######### KNeighborsClassifier #########
        estimator = KNeighborsClassifier()

        # Set the parameters by cross-validation
        # Change these parameters according to the choosen algorithm
        tuned_parameters = [{'n_neighbors': [2, 5, 10, 8], 'algorithm':['auto', 'ball_tree', 'kd_tree', 'brute']}]
        print("available params for the estimator:")
        print(estimator.get_params())
        grid_search = GridSearchCV(estimator, tuned_parameters, cv=3)
        grid_search.fit(X, y)
        print("Best score: %0.3f" % grid_search.best_score_)
        print("Best parameters set:")
        best_parameters = grid_search.best_params_
        print(best_parameters)

        # 3. fit
        self.logreg.fit(X, y)

    def getCurrentLine(self, GameState):
        pos = GameState.getPacmanPosition()
        blinky = GameState.getGhostPosition(1)
        pinky = GameState.getGhostPosition(2)
        food_list = GameState.getFood().asList()
        closestFood = (0, 0)
        closestFood = min(food_list, key=lambda x: manhattanDistance(x, pos))
        closestCaps = (0, 0)
        closestCaps = min(food_list, key=lambda x: manhattanDistance(x, pos))
        legal_moves = GameState.getLegalActions()

        current_line = [
            self.round,
            self.game,
            manhattanDistance(pos, blinky),
            GameState.getGhostState(1).scaredTimer,
            manhattanDistance(pos, pinky),
            GameState.getGhostState(2).scaredTimer,
            len(food_list),
            manhattanDistance(pos, closestFood),
            manhattanDistance(pos, closestCaps),
            "North" in legal_moves,
            "South" in legal_moves,
            "East" in legal_moves,
            "West" in legal_moves,
            GameState.getScore(),
            False
        ]

        return current_line

    def getAction(self, GameState):
        self.round += 1
        current_line = self.getCurrentLine(GameState)

        index = self.mlcheck(current_line)
        action = self.Revert[int(index[0])]
        if not action in GameState.getLegalActions():
            action = ExpectimaxAgent.getAction(self, GameState)

        win = GameState.generatePacmanSuccessor(action).isWin()
        if win :
            self.victory = True
        return action

    def mlcheck(self, line):
        new = DataFrame(columns=[
            'turn',
            'game',
            'Blinky_dist',
            'Blinky_scared_timer',
            'Pinky_dist',
            'Pinky_scared_timer',
            'food_number',
            'nearest_food_dist',
            'nearest_capsule_dist',
            'north',
            'south',
            'east',
            'west',
            'score',
            'game_won'
        ])
        new.loc[0, :] = line

        new.head()
        return self.logreg.predict(new)
