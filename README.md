# Multi agent PacMan

## Fonction d'évaluation

La fonction d'évaluation prend en compte quasiment toutes les informations disponibles dans l'état:
- L'état de PacMan
- L'état des fantômes
- La position des graines de nourriture
- La position des capsules
- Le nombre de graines encore présentes sur la carte
- Le score du joueur pour l'état actuel

Une information pouvant être jugée importante n'est pas prise en compte: la direction vers laquelle les fantômes sont tournés. En effet, la valeur ajoutée par la prise en compte de cette information est bien en dessous de l'effort représenté par la création d'un code d'évaluation pour celle ci.  
  
Les bouts de code effectuant le traitement des informations sont partiellement paramétrables: en effet, il a rapidement été constaté que les informations prises en compte ont beau être pertinantes, l'importance qu'on leur accorde peut complètement dérégler le comportement de l'IA. Ainsi, en plus du traitement de l'information, on retrouve sur le fichier `multiAgents.py` de la ligne 16 à la ligne 20, les multiplicateurs appliqués à chaque partie de la fonction d'évaluation.

**GHOST_IMPORTANCE_RATIO**: L'importance attribuée aux fantômes. Ce multiplicateur est à `0.4` car bien qu'il soit important que PacMan évite les fantômes, il est tout de même important qu'il consacre plus d'efforts à se nourir qu'à fuir les fantômes.  
  
**FOOD_IMPORTANCE_RATIO**: L'importance attribuée à la graine de nourriture la plus proche. Ce multiplicateur est à `1.2` de manière à ce que PacMan s'occupe de se diriger vers la graine la plus proche lorsqu'il n'a rien de mieux à faire (mieux étant, par exemple, aller manger un fantôme vulnérable).  
  
**CAPSULE_IMPORTANCE_RATIO**: L'importance attribuée aux capsules. Ce multiplicateur est à `1.0` de sorte que PacMan préfère s'intéresser à la nourriture, sauf quand il peut se faire beaucoup de points en mangeant la capsule pour manger un fantôme.  
  
**SCORE_IMPORTANCE_RATIO**: L'importance attribuée au score. Ce multiplicateur est à `10.0` de manière à ce que PacMan prenne toujours la décision qui lui rapportera le plus de points (avec un profondeur maximale, il serait possible d'utiliser uniquement ce paramètre pour récupérer à chaque fois le score maximal au jeu).  
  
**REMAINING_FOODS_IMPORTANCE_RATIO**: L'importance attribuée au nombre de graines de nourriture encore présente sur le terrain. Ce multiplicateur est à `7.0` afin qu'un des objectifs prioritaires de PacMan soit de réduire le nombre total de graines.  

## Problèmes rencontrés et solutions apportées

Lors de l'écriture de la fonction d'évaluation, bien des problèmes ont été rencontrés. Tout d'abord, cette fonction était exécutée très vite et de très nombreuses fois dans un court laps de temps, ce qui l'a rendue très compliquée à débugger. Fort heureusement, l'utilisation de la fonction `raw_input()` de python nous a permis d'isoler l'exécution des différentes parties du code afin de les comprendre et de les affiner.  
Ensuite, il a été difficile de trouver le meilleur équilibre pour que PacMan ne passe pas son temps à fuir les fantômes ou à se jeter sur eux. Ou encore faire en sorte qu'il reste mobile et cherche toujours à atteindre les graines restantes. Afin de tester très facilement les différentes configuration possibles, le système des multiplicateurs évoqués plus haut a été mis en place.  
Le principal problème deumeurant à ce stade est la lenteur d'exécution du code: en effet, bien qu'avec une profondeur de `2`, les résultats soient très acceptables, l'IA deviens quasiment parfaite aux delà de de la pronfondeur `4`. En revanche, une profondeur de `4` rends l'exécution du programme extrêmement lente.

## Algorithmes

### Minimax

Définition de wikipédia : L'algorithme minimax (aussi appelé algorithme MinMax) est un algorithme qui s'applique à la théorie des jeux1 pour les jeux à deux joueurs à somme nulle (et à information complète) consistant à minimiser la perte maximum (c'est-à-dire dans le pire des cas).

En résumé (et appliqué à notre cas d'étude Pacman), on regarde pour une profondeur de parcours de l'arbre donnée, les coups possibles & s'ils sont meilleurs ou pire.
A chaque coup joué par Pacman, on regarde avec une certaine profondeur les coups suivant & s'ils sont favorables à Pacman, on considère que ce sont des "max".
A l'inverse, si un fantome se rapproche trop ou que pacman a le choix entre manger de la nourriture (qui est aussi un "max" en soi) et fuir un fantome, il va choisir fuir un fantome.


### AlphaBeta

Il existe différents algorithmes basés sur MinMax permettant d'optimiser la recherche du meilleur coup en limitant le nombre de nœuds visités dans l'arbre de jeu, le plus connu est l'élagage alpha-bêta. 
En pratique, l'arbre est souvent trop vaste pour pouvoir être intégralement exploré (comme pour le jeu d'échecs ou de go). 
Seule une fraction de l'arbre est alors explorée. 

Dans le code, cela se traduit par l'introduction de 2 variables (que l'on a appellé alpha & beta ... :D) qui vont servir de check pour chaque noeud :
    - Si le coup suivant est "inférieur" à alpha par exemple, on va dire à l'algo que c'est bon
    - Beta prends la valeur du min entre sa propre valeur & la valeur du noeud (coup) joué 
    
et ainsi dessuite (récursivité, c'est à dire rappel de l'algorithme jusqu'à tombé sur un noeud terminal comme le minimax)


### Expectiminimax

Cet algorithme reprend le principe du minimax mais cette fois-ci, au lieu de parcourir l'arbre des possibilités avec un "max" & un "min", on affecte une valeur aux noeuds.
Cette valeur est un poid, une probabilité de choisir telle ou telle branche de l'arbre.
Dans notre cas, on retourne la somme des coûts des coups suivants divisé par leur nombre ce qui donne un "ratio" qui fait office de probabilité.

De plus, si on tombe sur un noeud final ou en profondeur 0 & on retourne l'heuristique qui permet de prendre une autre décision "intelligente".
L'algorithme est récursif c'est à dire qu'a chaque noeud visité on va rappeller l'algorithme en prenant le min ou le max selon si le coup est "adverse" (fantome va manger le Pacman) ou
"gagnant" (pacman est sauf + pacman mange un fruit OU pacman est sauf + pacman mange un fantome etc ...). 

# Machine Learning

## Création du dataset
___Notre dataset comprend les informations que nous avons jugé nécessaire :___

-	Decision : Contient la direction que le Pac-Man a choisi ce tour 

-	Turn : Contient le numéro du tour de la partie en cours

-	Game : Contient le numéro de la partie

-	Blinky_dist : Contient la distance de Manhattan entre le Pac-Man et le premier fantôme

-	Blinky_scared_timer : Contient le temps restant avant que le fantôme ne soit plus apeuré

-	Pinky_dist : Contient la distance de Manhattan entre le Pac-Man et le premier fantôme

-	Pinky_scared_timer : Contient le temps restant avant que le fantôme ne soit plus apeuré

-	Food_number : Contient le nombre de nourriture restante

-	Nearest_food_dist :	Contient la distance de Manhattan entre le Pac-Man et la nourriture la plus proche

-	Nearest_capsule_dist : Contient la distance de Manhattan entre le Pac-Man et la capsule la plus proche

-	North :	Contient 1 si le déplacement est légal et 0 si non

-	South : Contient 1 si le déplacement est légal et 0 si non

-	East : Contient 1 si le déplacement est légal et 0 si non

-	West : Contient 1 si le déplacement est légal et 0 si non

-	Score : Contient le score du tour actuel

-	Game_won : Contient 1 si la partie est gagné et 0 si non
Nous avons choisi d’inclure ces données pour permettre au Pac-Man d’avoir suffisamment d’informations sur son environnement tout en lui permettant de pouvoir changer de carte.

## Training Agent
Pour entrainer notre Pac-Man nous utilisons l’algorithme d’expectiminimax, cet algorithme reprend le principe du minimax mais au lieu de parcourir l'arbre des possibilités avec un "max" & un "min", on affecte une valeur aux noeuds. Cette valeur est un poid, une probabilité de choisir telle ou telle branche de l'arbre. Dans notre cas, on retourne la somme des coûts des coups suivants divisé par leur nombre ce qui donne un "ratio" qui fait office de probabilité.
De plus, si on tombe sur un noeud final ou en profondeur 0 & on retourne l'heuristique qui permet de prendre une autre décision "intelligente". L'algorithme est récursif c'est à dire qu'a chaque noeud visité on va rappeller l'algorithme en prenant le min ou le max selon si le coup est "adverse" (fantome va manger le Pacman) ou "gagnant" (pacman est sauf + pacman mange un fruit OU pacman est sauf + pacman mange un fantome etc ...).

## Machine Learning Agent
 
![](images/pic.png?raw=true "On a choisi les classifiers")

***Problèmes rencontrés*** 

Le plus gros problème a été de mettre à profit les résultats / les paramètres utilisés avec différents algorithmes dans un GridSearch (implémentation / utilisation de ces données dans notre propre jeu de données)

Nous avons scindé notre machine learning en deux parties : l'une avec un SVC & l'autre avec KNN (deux algorithmes permettant d'améliorer l'aprentissage). 
Le best score donné par le GridSearch donne environ 92% pour le SVC (très long à entrainer !!) & 40% pour le KNN avec l'algorithme ball_tree & 5 voisins (neighbors).
Le ball tree qui est lui même un algo relativement rapide qui parcourt un arbre à la recherche de voisins dans un "radius" (rayon) d'action donné. 

Malheureusement, notre pacman MachineLearningAgent n'atteind pas le niveau de l'Expectiminimax (+90% taux de victoires) avec 1 seule victoire à son actif !

***Choix effectués***

Nous avons également décidé de sauvegarder (CSV) uniquement les parties avec score > 1500 & status = "win" pour l'apprentissage.
Possible erreur que nous avons faite : manque d'entrainement, CSV de 30 000 lignes maximum sachant qu'une partie en prends environ 1000 en moyenne soit 30 parties.*
Notre dataset était au départ constitué des déplacements des fantomes en x & en y ainsi que pacman. 
Nous avons par la suite décidé de supprimer les coordonnées séparées pour les remplacer par la distance (pacman - Ghosts 1 et 2).
Pareil pour la nourriture.
Les résultats n'étaient pas si différents pour être honnête même inchangés. 

Le problème principal de l'agent MachineLearning est la gestion des fantomes : il meurt quasiment à 100% quand un fantome approche. 
On avait donc triché au départ en retournant l'algo Expectimax si le fantome est trop proche ! 

***Bilan*** 

Très difficile d'obtenir un score équivalent à l'Expectimax... Cependant on a pu apprendre à générer un dataset & lancer un agent qui prends des décisions (plutot mauvaises ici en particulier par rapport aux fantomes !)
ainsi que quelques algorithmes permettant de faire du machine learning même si ce n'est qu'un survol...



